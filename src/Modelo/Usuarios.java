package Modelo;
public class Usuarios {
    private int idUsuario;
    private String nombre;
    private String correo;
    private String contraseña;

    public Usuarios(int idUsuario, String nombre, String correo, String contraseña) {
        this.idUsuario = idUsuario;
        this.nombre = nombre;
        this.correo = correo;
        this.contraseña = contraseña;
    }

    public Usuarios() {
        this.idUsuario = 0;
        this.nombre = "";
        this.correo = "";
        this.contraseña = "";
    }
    
    public Usuarios( Usuarios aux) {
        this.idUsuario = aux.idUsuario;
        this.nombre = aux.nombre;
        this.correo = aux.correo;
        this.contraseña = aux.contraseña;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
    
    
}
